#!/usr/bin/python3

""" Programa que devuelve una pagina HTML con las urls de los videos de youtube de CursosWeb
"""

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import feedparser
import sys


class YoutubeHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False  # indica si estamos dentro de una etiqueta entry
        self.inTitle = False  # indica si estamos dentro de una etiqueta title
        self.inLinlk = False  # indica si estamos dentro de una etiqueta link
        self.title = ""  # variable que almacena el titulo
        self.link = ""  # variable que almacena la url del video
        self.videos = []  # lista que almacena los videos

    def startElement(self, name, attrs):
        """
        Método que se ejecuta cuando se abre una etiqueta
        :param name: nombre de la etiqueta
        :param attrs: diccionario con los atributos de la etiqueta
        """
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry and name == 'title':
            self.inTitle = True
        # Si encontramos una etiqueta link, entramos
        elif self.inEntry and name == 'link':
            self.inLinlk = True
            self.link = attrs.get('href')  # obtengo el valor del atributo 'href'

    def endElement(self, name):
        """
        Método que se ejecuta cuando se cierra una etiqueta
        :param name: nombre de la etiqueta
        """
        if name == 'entry':
            self.inEntry = False
            # Agregamos el título y enlace del video actual a la lista de videos
            self.videos.append({'title': self.title, 'link': self.link})
            self.title = ""  # reiniciamos el título del video actual
            self.link = ""  # reiniciamos el enlace del video actual
        elif self.inTitle and name == 'title':
            self.inTitle = False
        # encontramos el final de etiqueta link, salimos
        elif self.inLinlk and name == 'link':
            self.inLinlk = False

    def characters(self, chars):
        """
        Método que se ejecuta cuando se encuentra texto
        :param chars: texto encontrado
        """
        if self.inTitle:
            self.title = self.title + chars


# --- Main program ---
if __name__ == "__main__":
    """ Cargamos l archivo RSS que descargamos y guardamos en 'youtube.xml' """
    rss_url = "youtube.xml"
    feed = feedparser.parse(rss_url)

    # Verificamos si el análisis fue exitoso
    if feed.bozo == 0:  # Verificamos si no hubo errores de análisis
        """ Creamos el analizador XML y el manejador"""
        parser = make_parser()
        handler = YoutubeHandler()
        parser.setContentHandler(handler)

        """ Leemos el archivo RSS y lo analizamos """
        with open(rss_url, "r") as xmlFile:
            parser.parse(xmlFile)

        """ Generamos la pg HTML con los títulos y enlaces 
        de los videos de CursosWeb de youtube"""
        plantilla = ("<!DOCTYPE html>\n<html>\n<head>\n<title>CursosWeb Videos</title>\n</head>\n<body>\n<h1>Videos de "
                     "CursosWeb</h1>\n<ul>\n")
        html_output = plantilla
        for video in handler.videos:
            html_output += f"<li><a href=\"{video['link']}\">{video['title']}</a></li>\n"
        html_output += "</ul>\n</body>\n</html>"

        """ Guardamos la pg HTML en un archivo """
        with open("cursosweb_videos.html", "w") as f:
            f.write(html_output)

        print("La página HTML ha sido generada exitosamente.")
    else:
        print("Hubo un error al intentar analizar el archivo RSS.")
